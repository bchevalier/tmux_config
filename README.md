# tmux-config

**tmux-config** is a collection of tmux configuration files and plugins.

---

## Installation

Run the following commands:
```
$ cd ~
$ git clone git@gitlab.com:bchevalier/tmux_config.git
$ ln -s tmux_config/gpakosz_config/.tmux.conf .tmux.conf
$ ln -s tmux_config/tmux.conf.local .tmux.conf.local
```
