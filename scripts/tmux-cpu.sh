#!/usr/bin/env bash

WARN_THRESHOLD=25
ALARM_THRESHOLD=50

source ~/tmux-terminal-colors.sh

main() {
    local max_history_size=${1:-5}
    local is_tmux=${2:-true}

    # get cpu usage of all processes as a percentage
    local cpu_percent=$( ps -A -o %cpu | awk '{s+=$1} END {print s}' )
    local cpu_rounded=$( printf "%.0f" $cpu_percent )

    # get total availble cpu percentage
    local num_cpu=$( sysctl -n hw.ncpu )
    local total_cpu=$(($num_cpu * 100))

    # read in saved variables from last time script was executed
    source ~/.tmux-cpu

    if [ -z ${cpu_history+x} ];
    then
        # no historical data found so just create an array with one value
        cpu_history=( $cpu_rounded )
    else
        # check size of saved array
        local IFS=$' '
        while [ ${#cpu_history[@]} -ge ${max_history_size} ];
        do
           # remove first element of array
            cpu_history=( ${cpu_history[@]:1} )
        done

        # push onto back of array
        cpu_history=( "${cpu_history[@]}" $cpu_rounded )
    fi

    local normalized_alarm_threshold=$(( $ALARM_THRESHOLD * $num_cpu ))
    local normalized_warn_threshold=$(( $WARN_THRESHOLD * $num_cpu ))

    local sparks=''
    for val in "${cpu_history[@]}"
    do
        color=$( msg_color $val $normalized_alarm_threshold $normalized_warn_threshold $is_tmux )
        normalized_spark_array=$( spark 0 $val $total_cpu )
        normalized_spark="${normalized_spark_array[@]:1:1}"
        colored_spark=$( printf "%s%s" "$color" "$normalized_spark" )
        sparks="${sparks}${colored_spark}"
    done

    local current_color=$( msg_color $cpu_rounded $normalized_alarm_threshold $normalized_warn_threshold $is_tmux )
    if [ $is_tmux = true ];
    then
        printf "%sCPU: %s%% %s#[default]" "$current_color" "$cpu_percent" "$sparks"
    else
        printf "%sCPU: %s%% %s\n" "$current_color" "$cpu_percent" "$sparks"
    fi

    # save cpu queue for next time script is executed
    declare -p cpu_history > ~/.tmux-cpu
}

main "$@"
