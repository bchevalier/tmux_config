#!/usr/bin/env bash

main() {
  # get current pane command
  local cmd=$(tmux display-message -p "#{pane_current_command}")

  if [ $cmd = "ssh" ]; then
      # get the argument to the ssh command that was executed in current pane ( i.e. user@host )
      local arg=$( pgrep -flaP $( tmux display-message -p "#{pane_pid}") | cut -d" " -f3 )

      # strip off username if there is one
      local fqdn=$( echo $arg | cut -d"@" -f2 )

      # get shortname from fully qualified domain name
      local short_name=$( echo $fqdn | cut -d"." -f1 )
      echo $short_name
  else
    echo $( hostname )
  fi
}

main
