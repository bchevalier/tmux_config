#!/usr/bin/env bash

TMUX_GOOD_COLOR="#[fg=green,bright]"
TERM_GOOD_COLOR=$( printf "\e[1;32m" )

TMUX_WARN_COLOR="#[fg=yellow,bright]"
TERM_WARN_COLOR=$( printf "\e[1;33m" )

TMUX_ALRAM_COLOR="#[fg=red,bright]"
TERM_ALARM_COLOR=$( printf "\e[1;31m" )

msg_color() {
    local is_tmux=$4

    if [ $is_tmux = true ]
    then
        tmux_msg_color $1 $2 $3
    else
        terminal_msg_color $1 $2 $3
    fi
}

tmux_msg_color() {
    local value=$1
    local alarm_threshold=$2
    local warn_threshold=$3

    if [ $value -gt $alarm_threshold ]
    then
        echo $TMUX_ALARM_COLOR
    elif [ $value -lt $warn_threshold ]
    then
        echo $TMUX_GOOD_COLOR
    else
        echo $TMUX_WARN_COLOR
    fi
}

terminal_msg_color() {
    local value=$1
    local alarm_threshold=$2
    local warn_threshold=$3

    if [ $value -gt $alarm_threshold ]
    then
        echo $TERM_ALARM_COLOR
    elif [ $value -lt $warn_threshold ]
    then
        echo $TERM_GOOD_COLOR
    else
        echo $TERM_WARN_COLOR
    fi
}
