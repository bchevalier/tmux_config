#!/usr/bin/env bash

WARN_THRESHOLD=50
ALARM_THRESHOLD=90

source ~/tmux-terminal-colors.sh

main() {
    local max_history_size=${1:-5}
    local is_tmux=${2:-true}
    local used_bytes=0
    local rows=$(vm_stat)
    local IFS=$'\n'
    for x in $rows;
    do
        local IFS=$':'
        # split row on the colon
        tokens=( $x )
        label=${tokens[0]}
        if [ "$label" == "Pages active" ] || [ "$label" == "Pages wired down" ]; then
            # strip leading whitespace using xargs
            value=$( echo ${tokens[1]} | xargs )

            # trim last character
            bytes=${value::${#value}-1}

            used_bytes="$(( used_bytes + bytes * 4096 ))"
        fi
    done

    # get used memory as a decimal in gigabytes
    local used_gb="$( echo "scale=2;${used_bytes} / 1024 / 1024 / 1024" | bc)"

    # get total available memory in bytes
    local total_bytes=$( sysctl -n hw.memsize )

    # get used memory as a normalized integer between 0 and 100
    local used_normalized="$( printf "%.0f" $( echo "scale=1; ${used_bytes} / ${total_bytes} * 100" | bc ) )"

    # read in saved variables from last time script was executed
    source ~/.tmux-mem

    if [ -z ${mem_history+x} ];
    then
        # no historical data found so just create an array with one value
        mem_history=( $used_normalized )
    else
        # check size of saved array
        local IFS=$' '
        while [ ${#mem_history[@]} -ge ${max_history_size} ];
        do
           # remove first element of array
            mem_history=( ${mem_history[@]:1} )
        done

        # push onto back of array
        mem_history=( "${mem_history[@]}" $used_normalized )
    fi

    local sparks=''
    for val in "${mem_history[@]}"
    do
        color=$( msg_color $val $ALARM_THRESHOLD $WARN_THRESHOLD $is_tmux )
        normalized_spark_array=$( spark 0 $val 100 )
        normalized_spark="${normalized_spark_array[@]:1:1}"
        colored_spark=$( printf "%s%s" "$color" "$normalized_spark" )
        sparks="${sparks}${colored_spark}"
    done

    local current_color=$( msg_color $used_normalized $ALARM_THRESHOLD $WARN_THRESHOLD $is_tmux )
    if [ $is_tmux = true ];
    then
        printf "%sRAM: %sgb %s#[default]" "$current_color" "$used_gb" "$sparks"
    else
        printf "%sRAM: %sgb %s\n" "$current_color" "$used_gb" "$sparks"
    fi

    # save memory queue for next time script is executed
    declare -p mem_history > ~/.tmux-mem
}

main "$@"
