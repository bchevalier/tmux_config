_circled_digit() {
    local circled_digits=( ⓪ ① ② ③ ④ ⑤ ⑥ ⑦ ⑧ ⑨ ⑩ ⑪ ⑫ ⑬ ⑭ ⑮ ⑯ ⑰ ⑱ ⑲ ⑳ )
    if [ "$1" -le 20 ] 2>/dev/null;
    then
        echo "${circled_digits[$1]}"
    else
        echo "$1"
    fi
}

_hostname() {
    local tty="${1:-$(tmux display -p '#{pane_tty}')}"
    if [ x"$OSTYPE" = x"cygwin" ]; then
        pid=$(ps -a | awk -v tty="${tty##/dev/}" '$5 == tty && /ssh/ && !/autossh/ && !/-W/ { print $1 }')
        [ -n "$pid" ] && ssh_parameters=$(tr '\0' ' ' < "/proc/$pid/cmdline" | sed 's/^ssh //')
    else
        ssh_parameters=$(ps -t "$tty" -o command= | awk '/ssh/ && !/autossh/ && !/-W/ { $1=""; print $0; exit }')
    fi
    if [ -n "$ssh_parameters" ]; then
        # shellcheck disable=SC2086
        hostname=$(ssh -G $ssh_parameters 2>/dev/null | awk 'NR > 2 { exit } ; /^hostname / { print $2 }')
        # shellcheck disable=SC2086
        [ -z "$hostname" ] && hostname=$(ssh -T -o ControlPath=none -o ProxyCommand="sh -c 'echo %%hostname%% %h >&2'" $ssh_parameters 2>&1 | awk '/^%hostname% / { print $2; exit }')
        #shellcheck disable=SC1004
        hostname=$(echo "$hostname" | awk '\
        { \
            if ($1~/^[0-9.:]+$/) \
                print $1; \
            else \
                split($1, a, ".") ; print a[1] \
        }')
    else
        hostname=$(command hostname -s)
    fi

    echo "$hostname"
}

_toggle_mouse() {
    old=$( tmux show -g | grep mouse | head -n 1 | cut -d' ' -f2 )
    new=""

    if [ "$old" = "on" ];
    then
        new="off"
    else
        new="on"
    fi

    tmux set -g mouse $new\;
    tmux display "mouse: $new"

    # send escape key to ensure scroll mode exits if previously in mouse mode
    tmux send-keys $'\e'
}

_battery() {
    local battery_bar_length=${1:-5}
    local battery_bar_symbol_full='◼'
    local battery_bar_symbol_empty='◻'

    local uname_s=$(uname -s)
    case "$uname_s" in
        *Darwin*)
            batt=$(pmset -g batt)
            percentage=$(echo "$batt" |egrep -o [0-9]+%) || return
            discharging=$(echo "$batt" | grep -qi "discharging" && echo "true" || echo "false")
            charge="${percentage%%%} / 100"
            ;;
        *Linux*)
            batpath=/sys/class/power_supply/BAT0
            if [ ! -d $batpath ]; then
                batpath=/sys/class/power_supply/BAT1
            fi
            discharging=$(grep -qi "discharging" $batpath/status && echo "true" || echo "false")
            bat_capacity=$batpath/capacity
            bat_energy_full=$batpath/energy_full
            bat_energy_now=$batpath/energy_now
            if [ -r "$bat_capacity" ]; then
                charge="$(cat $bat_capacity) / 100"
            else
                if [ ! -r "$bat_energy_full" ] || [ ! -r "$bat_energy_now" ]; then
                    return
                fi
                charge="$(cat $bat_energy_now) / $(cat $bat_energy_full)" || return
            fi
            ;;
        *CYGWIN*)
            wmic path Win32_Battery 2>&1 | grep -q 'No Instance' && return
            discharging=$(wmic path Win32_Battery Get BatteryStatus 2>/dev/null | grep -q 1 && echo "true" || echo "false")
            percentage=$(wmic path Win32_Battery Get EstimatedChargeRemaining /format:list 2>/dev/null | grep '[^[:blank:]]' | cut -d= -f2)
            charge="${percentage} / 100"
            ;;
        *OpenBSD*)
            discharging=$(sysctl -n hw.sensors.acpibat0.raw0 | grep -q 1 && echo "true" || echo "false")
            if sysctl -n hw.sensors.acpibat0 | grep -q amphour; then
                charge="$(sysctl -n hw.sensors.acpibat0.amphour3 | cut -d' ' -f1) / $(sysctl -n hw.sensors.acpibat0.amphour0 | cut -d' ' -f1)"
            else
                charge="$(sysctl -n hw.sensors.acpibat0.watthour3 | cut -d' ' -f1) / $(sysctl -n hw.sensors.acpibat0.watthour0 | cut -d' ' -f1)"
            fi
            ;;
        *)
            return
    esac

    if [ x"$discharging" = x"true" ]; then
        battery_status="🔋"
    else
        battery_status="⚡"
    fi

    palette="196 202 208 214 220 226 190 154 118 82 46"
    palette=$(echo "$palette" | awk -v n="$battery_bar_length" '{ for (i = 0; i < n; ++i) printf $(1 + (i * NF / n))" " }')

    full=$(awk "BEGIN { printf \"%.0f\", ($charge) * $battery_bar_length }")
    battery_bar=""
    [ "$full" -gt 0 ] && \
        battery_bar="$battery_bar$(printf "#[fg=colour%s]$battery_bar_symbol_full" $(echo "$palette" | cut -d' ' -f1-"$full"))"

    empty=$((battery_bar_length - full))
    [ "$empty" -gt 0 ] && \
        battery_bar="$battery_bar$(printf "#[fg=colour%s]$battery_bar_symbol_empty" $(echo "$palette" | cut -d' ' -f$((full + 1))-$((full + empty))))"

    # update percentage color to match current battery level
    if [ "$full" -gt 0 ]
    then
        current_palette_level=$( echo "$palette" | cut -d' ' -f $full )
    else
        current_palette_level=$( echo "$palette" | cut -d' ' -f 1 )
    fi
    percentage=$( printf "#[fg=colour%s]%s" $current_palette_level $percentage )

    echo "$battery_status $battery_bar $percentage"
}

_msg_color() {
    local value=$1
    local alarm_threshold=$2
    local warn_threshold=$3

    if [ $value -gt $alarm_threshold ]
    then
        echo "#[fg=red,bright]"
    elif [ $value -lt $warn_threshold ]
    then
        echo "#[fg=green,bright]"
    else
        echo "#[fg=yellow,bright]"
    fi
}

_memory() {
    local max_history_size=${1:-5}
    local warn_threshold=50
    local alarm_threshold=90
    local used_bytes=0
    local rows=$(vm_stat)
    local IFS=$'\n'
    for x in $rows;
    do
        local IFS=$':'
        # split row on the colon
        tokens=( $x )
        label=${tokens[0]}
        if [ "$label" == "Pages active" ] || [ "$label" == "Pages wired down" ]; then
            # strip leading whitespace using xargs
            value=$( echo ${tokens[1]} | xargs )

            # trim last character
            bytes=${value::${#value}-1}

            used_bytes="$(( used_bytes + bytes * 4096 ))"
        fi
    done

    # get used memory as a decimal in gigabytes
    local used_gb="$( echo "scale=2;${used_bytes} / 1024 / 1024 / 1024" | bc)"

    # get total available memory in bytes
    local total_bytes=$( sysctl -n hw.memsize )

    # get used memory as a normalized integer between 0 and 100
    local used_normalized="$( printf "%.0f" $( echo "scale=1; ${used_bytes} / ${total_bytes} * 100" | bc ) )"

    # read in saved variables from last time script was executed
    source ~/.tmux-mem

    if [ -z ${mem_history+x} ];
    then
        # no historical data found so just create an array with one value
        mem_history=( $used_normalized )
    else
        # check size of saved array
        local IFS=$' '
        while [ ${#mem_history[@]} -ge ${max_history_size} ];
        do
           # remove first element of array
            mem_history=( ${mem_history[@]:1} )
        done

        # push onto back of array
        mem_history=( "${mem_history[@]}" $used_normalized )
    fi

    local sparks=''
    for val in "${mem_history[@]}"
    do
        color=$( _msg_color $val $alarm_threshold $warn_threshold )
        normalized_spark_array=$( spark 0 $val 100 )
        normalized_spark="${normalized_spark_array[@]:1:1}"
        colored_spark=$( printf "%s%s" "$color" "$normalized_spark" )
        sparks="${sparks}${colored_spark}"
    done

    local current_color=$( _msg_color $used_normalized $alarm_threshold $warn_threshold )
    printf "%sRAM: %sgb %s#[default]" "$current_color" "$used_gb" "$sparks"

    # save memory queue for next time script is executed
    declare -p mem_history > ~/.tmux-mem
}

_cpu() {
    local max_history_size=${1:-5}
    local warn_threshold=25
    local alarm_threshold=50

    # get cpu usage of all processes as a percentage
    local cpu_percent=$( ps -A -o %cpu | awk '{s+=$1} END {print s}' )
    local cpu_rounded=$( printf "%.0f" $cpu_percent )

    # get total availble cpu percentage
    local num_cpu=$( sysctl -n hw.ncpu )
    local total_cpu=$(($num_cpu * 100))

    # read in saved variables from last time script was executed
    source ~/.tmux-cpu

    if [ -z ${cpu_history+x} ];
    then
        # no historical data found so just create an array with one value
        cpu_history=( $cpu_rounded )
    else
        # check size of saved array
        local IFS=$' '
        while [ ${#cpu_history[@]} -ge ${max_history_size} ];
        do
           # remove first element of array
            cpu_history=( ${cpu_history[@]:1} )
        done

        # push onto back of array
        cpu_history=( "${cpu_history[@]}" $cpu_rounded )
    fi

    local normalized_alarm_threshold=$(( $alarm_threshold * $num_cpu ))
    local normalized_warn_threshold=$(( $warn_threshold * $num_cpu ))

    local sparks=''
    for val in "${cpu_history[@]}"
    do
        color=$( _msg_color $val $normalized_alarm_threshold $normalized_warn_threshold )
        normalized_spark_array=$( spark 0 $val $total_cpu )
        normalized_spark="${normalized_spark_array[@]:1:1}"
        colored_spark=$( printf "%s%s" "$color" "$normalized_spark" )
        sparks="${sparks}${colored_spark}"
    done

    local current_color=$( _msg_color $cpu_rounded $normalized_alarm_threshold $normalized_warn_threshold )
    printf "%sCPU: %s%% %s#[default]" "$current_color" "$cpu_percent" "$sparks"

    # save cpu queue for next time script is executed
    declare -p cpu_history > ~/.tmux-cpu
}
